package com.caizhao.myrecyclerviewapplication;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private DrawerLayout mdrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // for toobar menu
//        setContentView(R.layout.drawer_toolbar);
        setContentView(R.layout.myrecyclerview_layout);

        // for Recyclerview
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        MyAdapter myAdapter = new MyAdapter(this, NatureModel.getObjectList());
        recyclerView.setAdapter(myAdapter);
     //  LinearLayoutManager layoutManager = new LinearLayoutManager(this);
      //  layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

// for toobar menu
//        setupToolbarMenu();
//        setupDrawerLayout();
    }


    private void setupToolbarMenu() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("Navigation View");

    }

    private void setupDrawerLayout() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        mdrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this,
                mdrawerLayout,
                mToolbar,
                R.string.drawer_open,
                R.string.drawer_close);
        mdrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

    }

}
