package com.caizhao.myrecyclerviewapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private List<NatureModel> objectList;
    private LayoutInflater inflater;

    public MyAdapter(Context context, List<NatureModel> objectList) {
        inflater = LayoutInflater.from(context);
        this.objectList = objectList;

    }


    @Override
    public MyViewHolder onCreateViewHolder( ViewGroup parent, int i) {

        View view = inflater.inflate(R.layout.list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder( MyViewHolder myViewHolder, int position ) {
        NatureModel natureModel = objectList.get(position);
        myViewHolder.setData(natureModel, position);
    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView title;
        private ImageView imgThumb, imgDelete, imgCopy;
        private int position;
        private NatureModel currentObject;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_title);
            imgThumb = (ImageView) itemView.findViewById(R.id.img_thumb);
            imgDelete = (ImageView) itemView.findViewById(R.id.imag_delete);
            imgCopy = (ImageView) itemView.findViewById(R.id.imag_copy);
        }
        public void  setData( NatureModel natureModel, int position ){
            this.title.setText(natureModel.getTitle());
            this.imgThumb.setImageResource(natureModel.getImageId());
            this.position = position;
            this.currentObject = natureModel;
        }
    }

}
