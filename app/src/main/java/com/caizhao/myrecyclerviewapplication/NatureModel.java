package com.caizhao.myrecyclerviewapplication;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class NatureModel {
    private int imageId;
    private String title;

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static List<NatureModel> getObjectList(){
        List<NatureModel> dataList = new ArrayList<NatureModel>();
        int[] images = getImages();
        for (int i=0; i < images.length; i++){
            NatureModel nature = new NatureModel();
            nature.setImageId(images[i]);
            nature.setTitle("Picture " + i);
            dataList.add(nature);
        }

        return dataList;

    }


    private static int[]  getImages() {
        int[] images = {
                R.drawable.icon0, R.drawable.icon1,
                R.drawable.icon2, R.drawable.icon3,
                R.drawable.icon4, R.drawable.icon5,
                R.drawable.icon6, R.drawable.icon7,
                R.drawable.icon8, R.drawable.icon9,
        };
        return images;
    }

}
